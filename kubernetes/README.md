**LoadBalancer Installation:**

**    Installing MetalLB using default manifests**

        kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml

        **Wait until the MetalLB pods (controller and speakers) are ready:**

        kubectl wait --namespace metallb-system \
                --for=condition=ready pod \
                --selector=app=metallb \
                --timeout=90s

        **Setup address pool used by loadbalancers 🔗︎**

            docker network inspect -f '{{.IPAM.Config}}' kind  #Check Ip address of Docker network

            apiVersion: metallb.io/v1beta1
            kind: IPAddressPool
            metadata:
              name: example
              namespace: metallb-system
            spec:
              addresses:
              - 172.19.255.200-172.19.255.250
            ---
            apiVersion: metallb.io/v1beta1
            kind: L2Advertisement
            metadata:
              name: empty
              namespace: metallb-system
            
--------------------------------------------------------------------------------------------------------------------

**Installing Ingress controller**
  **Ingress NGINX 🔗︎**

    Setting Up An Ingress Controller
      We can leverage KIND’s extraPortMapping config option when creating a cluster to forward ports from the host to an ingress
      controller running on a node.

      We can also setup a custom node label by using node-labels in the kubeadm InitConfiguration, to be used by the ingress
      controller nodeSelector.

      1.Create a cluster with extraPortMapping for ingress

          cat <<EOF | kind create cluster --config=-
          kind: Cluster
          apiVersion: kind.x-k8s.io/v1alpha4
          nodes:
          - role: control-plane
            kubeadmConfigPatches:
            - |
              kind: InitConfiguration
              nodeRegistration:
                kubeletExtraArgs:
                  node-labels: "ingress-ready=true"
            extraPortMappings:
            - containerPort: 80
              hostPort: 80
              protocol: TCP
            - containerPort: 443
              hostPort: 443
              protocol: TCP
          EOF
          

      2.Deploy an Ingress controller, the following ingress controllers are known to work:

        - Ingress NGINX

          kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

          # Check the ingress is ready?

          kubectl wait --namespace ingress-nginx \
          --for=condition=ready pod \
          --selector=app.kubernetes.io/component=controller \
          --timeout=90s

          # Create Ingress Rule

              apiVersion: networking.k8s.io/v1
              kind: Ingress
              metadata:
                name: test
                annotations:
                  nginx.ingress.kubernetes.io/rewrite-target: /$2
              spec:
                rules:
                - http:
                    paths:
                    - pathType: Prefix
                      path: /test2(/|$)(.*)
                      backend:
                        service:
                          name: test-2
                          port:
                            number: 80
                    - pathType: Prefix
                      path: /test(/|$)(.*)
                      backend:
                        service:
                          name: test
                          port:
                            number: 80

                      # should output "foo-app"
                      curl localhost/test2
                      # should output "bar-app"
                      curl localhost/test
  
-----------------------------------kubectl config view --raw   -----------------------------------------------

  Execute the following command, it will set a clusterrole as cluster-admin which will give you the required access.

    kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous


    Access the Kind with addmin insecure-skip-tls-verify: true in kube/config

**kubectl config view --raw**

    apiVersion: v1
clusters:
- cluster:
    server: https://**3.110.65.132**:6443
**    insecure-skip-tls-verify:**
  name: kind-test
contexts:
- context:
    cluster: kind-test
    user: kind-test
  name: kind-test
current-context: kind-test
kind: Config
preferences: {}
users:
- name: kind-test
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURJVENDQWdtZ0F3SUJBZ0lJVkNvanFFdjZpak13RFFZSktvWklodmNOQVFFTEJRQXdGVEVUTUJFR0ExVUUKQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TXpBNE1qa3dNalUwTkRWYUZ3MHlOREE0TWpnd01qVTBORGxhTURReApGekFWQmdOVkJBb1REbk41YzNSbGJUcHRZWE4wWlhKek1Sa3dGd1lEVlFRREV4QnJkV0psY201bGRHVnpMV0ZrCmJXbHVNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXlGcE5tRWhOMUpQODlCWWwKb1BTMUZGNE9yc3N3dnY3Y25YVng3enh4MGE0cDAvL1VzUlhoamdueGc4dndWQ0ZyNzJ1MFlwb3ROdmgvdjREWgpmR0E4WnE2UDh0YXVVOHk0M1J3M3JBSWFYUmk4YzZOYloyaWlEV1owZGkwZCtqR0w3OEZ6dFVpazhFaW1Wc0VICjJrSVZkN0xZc1pDb3YrTm5uOGtjTmxwOGlxcGJ4NytxOFBIaFNuclBHUTBHc3p0aGpvMmVLRWhHakxzVEQwV1kKYW5Vc0lMcE1nenhNSWRxTldEdDlPV2gzVDl0Uk1IMng4Ujl1QTRQdWEwTUcxL09hUXRQZCtYRzRKam44V1ZlSQp0VjNFWklHRkttS24vbWZ4MW5lY2ZMVmNnRmg0VzJqcEUzdXBFNnJjRlNZRUl1QzBnSVU4a3pHQ250NzRhNklLClZ2WWxIUUlEQVFBQm8xWXdWREFPQmdOVkhROEJBZjhFQkFNQ0JhQXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUgKQXdJd0RBWURWUjBUQVFIL0JBSXdBREFmQmdOVkhTTUVHREFXZ0JRLzN5MU4zamp3M00yR29zMDZLajV3cnFsZgpaVEFOQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBRzJtckFpUW5TQlZHNVBDbFlxcmt0czM4QnpDZ3VhYS9nZGVPCnlkMTVhMFdGSm5MZjR0YkdhL3BJbWwyaWZvdGMvS1ZPajdZRlY1NlVPcUM3Tnd6ZlBrTDBtWFY2TGJwN0wzdlAKeUpVQVZSbWV6YzU2MUl6SDU4QlpabExMcjBqSE1IdGo1SHg4SlpsS3l1aWU0d05BUGU0d2VON1QvZXhyLzl6awpIdWdvYUZ6NUVuSU55ZVBWb2dPRWNWemtYWlVhQmVqN2NUa2Y0V2pRM0dyQThnNHFRc09YOVhEcHZJZDBwKzVqCkhDM2gweTZvN2lZa1F5YW5IQ293SDlLWExDaUtSZ0JneWtockFUTmNBUkFkZTJKdmRLTXBzSzZ2WDloVFNqYzQKVVUwbzhUL0l2a0cvdTNNMnJqQkhUQVo5MGxCNFloWXVlcVYvTFlzOUgxR1FJdytGWkE9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcEFJQkFBS0NBUUVBeUZwTm1FaE4xSlA4OUJZbG9QUzFGRjRPcnNzd3Z2N2NuWFZ4N3p4eDBhNHAwLy9VCnNSWGhqZ254Zzh2d1ZDRnI3MnUwWXBvdE52aC92NERaZkdBOFpxNlA4dGF1VTh5NDNSdzNyQUlhWFJpOGM2TmIKWjJpaURXWjBkaTBkK2pHTDc4Rnp0VWlrOEVpbVZzRUgya0lWZDdMWXNaQ292K05ubjhrY05scDhpcXBieDcrcQo4UEhoU25yUEdRMEdzenRoam8yZUtFaEdqTHNURDBXWWFuVXNJTHBNZ3p4TUlkcU5XRHQ5T1doM1Q5dFJNSDJ4CjhSOXVBNFB1YTBNRzEvT2FRdFBkK1hHNEpqbjhXVmVJdFYzRVpJR0ZLbUtuL21meDFuZWNmTFZjZ0ZoNFcyanAKRTN1cEU2cmNGU1lFSXVDMGdJVThrekdDbnQ3NGE2SUtWdllsSFFJREFRQUJBb0lCQVFDdEg0cnBTYUVuVFhGSQpvVktHa0sxZ0doNXAzOTRSVER6cU9xL20zNzRpUjNLUlpBRXZ2QTZaSS9KenhtZTI2dUpQa2l2ZkFBYU45NUM3CnMrVTZXaFlYVEVQS3ZNTUtXbDVyZ1pTQnA1ZU1WbkdSSXRrNnMrbEQxd0pQazVEUnZycUN4QkR0MXdYZk00ZTQKY3pnTDJ3cWQwZ2I0K0YwNDRwR0tPc2VNR0FpQjhFV0NheTIvQnhCOXFJdnlGaDJFcksveVc2bW5GSHVOU3JvMgpIakRNWlpPZVFHdkdRNkJ4bm91TlJ5d0g3ZFVFRFNDeDI2OEJSNDNvK1hmT1lTUFFQbVVoSUE2MVB3RHZUUFBkCk1oM29GUFJhL3hrS2NJc1ozUEF1R1NKWVFnSThLb0lOVnNYQmFROVArcUxtdERydXVMbkExemtxQ3IyUUFUdVIKY1BhcnBwb3BBb0dCQU1zWXYyKy9LdGcwT0N5N1drcDhRQ3A5RWFPa0NnVHF3eHR2ck1qRzgzSzJ2VnNDTFUvegozSUhYaGlFNG44Qm9XMStZOEJ6VHBOKzRBQzdYSEY3dVlEZU93SE1VanZmM01qWE5ud0pYSkdoU3k5b0xOeFRWClRCNUJJbktFRkVLb2lnRUZaclZFbVQ2ZlkxTFlqbkRDUjBWTTlRdThweDVIR0lNK01qNzRNV212QW9HQkFQeUsKbEg4SGVyWmJjbzNaU2xSeUxEbHVycUxWQ3hTbEhSbjhLSWkxVU91TVJadGdCbC9zeXMyd2M1SHlVKzlOc1BGLwpSRThrTVp0RVFlWHVrNFQ2N3d3cDYvc1lMVzRFTS9haWpMNGZ0Lzg1MCtJL2FhNDRDUk14TCtCVEpjdXNKd3FRCkNqd3BUUjI4UStnWEh0ZUdjbmNEMGxPa0J6VHRnSVJ4NXlNVFYyenpBb0dCQUloUVpQVUkxeVhqN3F3L1YzdlUKNTNCZEg0QzQ4aXlZc1RseC9BT0tYUTNxank5TlI4SjVCOUJ5S3NtVGFzM0dmVVd0cmNON1hoYjJOZEU2ZWdWUgpjMnoydUQ3RlZxd2gweVovaXpFVDhoQy9PYWx3aTVqRkRPRGxrTXhSMEJOcGNhNzd4Qkw0SnU3OEpic09OSlNFCklUZm5UaldCZXAxODFId0tPOGs5NWxSckFvR0FSbkRCQ3MyalJNb3RJYlBDTDl2SXlodzlTZDN5d1VkczNwcWYKRThLL1YzUkExaDNxcVViN0tuUlZTQmczb1VBaVlyb0U2NjVFeC9JNkQxQ0VoVXNZOG1PZkZ1S2ZpVmhTeGpDQQo0V2NuZzJFMTRKMmZkS0RjcHlYSkhPRVVZY01iQUs4OHE1czVqMW03OXppN3VaSWV5Mk9kZndiNW02SERmeTNtCk1ycHFZRkVDZ1lBSGRxWWNVeWlSUjlEV1lpeU9BWmVSZDlDbHE3ZmdtMTh6emtOSElrdWYrRWJkRzdicFNTaDMKSEU1R3FCc1UrWDVUMUllNWVmOFNnNVR4UkFqS1hWbGhTcDF3OHBBRVg4cHFiYkFiZm5aQnY5TXdRMm5PVXZnKwpQQk9VZHZzWmd0NldQSG1zTUJxNktMdk1LL2dCNEJzYXU2K1hCYVg5VHRHZVBzckIyeWRUM1E9PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=
