NGINX Controller Repo:

        https://github.com/nginxinc/kubernetes-ingress/tree/v3.3.0/deployments



Installtion Steps:


1. Configure RBAC and deployed nginx Controller

kubectl apply -f https://gitlab.com/ravikushwaha.official/Kubernetes-Installation-local/-/raw/main/nginx%20controller%20with%20CRDs/nginx.yaml?ref_type=heads

2. Install CRDs 

kubectl apply -f https://gitlab.com/ravikushwaha.official/Kubernetes-Installation-local/-/raw/main/nginx%20controller%20with%20CRDs/crds.yaml?ref_type=heads

3. Create default secret

kubectl apply -f https://gitlab.com/ravikushwaha.official/Kubernetes-Installation-local/-/raw/main/nginx%20controller%20with%20CRDs/default_secret.yaml

4. See pods in Running or Node

    kubectl get pods --namespace=nginx-ingress

5. Create a Service for the NGINX Ingress Controller Pods

    There are few ways:

        a. For GCP and Azure

        kubectl apply -f https://gitlab.com/ravikushwaha.official/Kubernetes-Installation-local/-/raw/main/nginx%20controller%20with%20CRDs/service/loadbalancer.yaml?ref_type=heads

        b. For AWS

        kubectl apply -f https://gitlab.com/ravikushwaha.official/Kubernetes-Installation-local/-/raw/main/nginx%20controller%20with%20CRDs/service/loadbalancer-aws-elb.yaml?ref_type=heads


6. Add the following keys to the config map file nginx-config.yaml

proxy-protocol: "True"
real-ip-header: "proxy_protocol"
set-real-ip-from: "0.0.0.0/0"



