#!/bin/sh
sudo yum update -y
sudo install docker -y
sudo systemctl start docker
sudo systemctl enable docker
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /usr/bin/kubectl
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo chmod +x ./minikube-linux-amd64
sudo mv ./minikube-linux-amd64 /usr/bin/minikube
sudo yum install conntrack -y
sudo usermod -aG docker $USER
newgrp docker
