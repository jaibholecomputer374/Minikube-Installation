https://github.com/kubernetes-sigs/cloud-provider-kind/tree/mainhttps://github.com/kubernetes-sigs/cloud-provider-kind/tree/main


kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  apiServerAddress: "0.0.0.0"
  apiServerPort: 6443
kubeadmConfigPatches:
- |
  kind: ClusterConfiguration
  apiServer:
    extraArgs:
      cloud-provider: "external"
      v: "5"
  controllerManager:
    extraArgs:
      cloud-provider: "external"
      v: "5"
  ---
  kind: InitConfiguration
  nodeRegistration:
    kubeletExtraArgs:
      cloud-provider: "external"
  ---
  kind: JoinConfiguration
  nodeRegistration:
    kubeletExtraArgs:
      cloud-provider: "external"
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 8080
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
- role: worker
- role: worker
#a cluster with 3 control-plane nodes and 3 workers
#kind: Cluster
#apiVersion: kind.x-k8s.io/v1alpha4
#nodes:
#- role: control-plane
#- role: control-plane
#- role: worker
#- role: worker