**Repo :**

    1. https://gateway-api.sigs.k8s.io/guides/?h=install+g

    2. https://docs.konghq.com/kubernetes-ingress-controller/latest/deployment/k4k8s/

    3. https://docs.konghq.com/kubernetes-ingress-controller/2.12.x/deployment/eks/

    4. https://docs.konghq.com/kubernetes-ingress-controller/2.12.x/guides/getting-started/

**Install Gateway API CRDs**

    kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v0.8.1/standard-install.yaml

**Install Kong controller/Gateway controller Open Source**

    kubectl apply -f https://raw.githubusercontent.com/Kong/kubernetes-ingress-controller/v2.12.0/deploy/single/all-in-one-dbless.yaml

**Create GatewayClass and Gateway using **

    kubectl apply -f https://gitlab.com/ravikushwaha.official/Kubernetes-Installation-local/-/raw/main/kong%20controller(Gateway%20&%20Ingress)%20Open%20Source/creategatewayclass&gateway.yaml
