**Installtion Repo:**

https://github.com/nginxinc/nginx-gateway-fabric/tree/main/deploy/manifests

https://github.com/nginxinc/nginx-gateway-fabric/blob/main/docs/installation.md


**User Guide:**

https://gateway-api.sigs.k8s.io/guides/


**Steps to Install nginx Gateway:**


    1. Install the Gateway API it will create under gateway-system namespace

        kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v0.8.1/standard-install.yaml

    2. Then install crds

        kubectl apply -f https://raw.githubusercontent.com/nginxinc/nginx-gateway-fabric/main/deploy/manifests/crds/gateway.nginx.org_nginxgateways.yaml

    3. Deploy the NGINX Gateway Fabric it will creae under nginx-gateway namespace

        kubectl apply -f https://raw.githubusercontent.com/nginxinc/nginx-gateway-fabric/main/deploy/manifests/nginx-gateway.yaml

    4. Confirm the NGINX Gateway Fabric is running in nginx-gateway

        kubectl get pods -n nginx-gateway

    5. Expose NGINX Gateway Fabric

        There are three ways to Expose:

            a. Node Port

            kubectl apply -f https://raw.githubusercontent.com/nginxinc/nginx-gateway-fabric/main/deploy/manifests/service/nodeport.yaml

            b. AWS Loadbalancer with NLB

            kubectl apply -f https://raw.githubusercontent.com/nginxinc/nginx-gateway-fabric/main/deploy/manifests/service/loadbalancer-aws-nlb.yaml

            C. Loadbalancer - Use this for GCP and Azure Loadbalancer

            kubectl apply -f https://raw.githubusercontent.com/nginxinc/nginx-gateway-fabric/main/deploy/manifests/service/loadbalancer.yaml


**Forward Nginx controller on localho**st

    kubectl -n nginx-gateway port-forward <pod-name> 8080:80 8443:443

    Change localhost on below path sudo vi /etc/hosts
