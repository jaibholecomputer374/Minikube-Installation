#!/bin/bash -xe
sudo yum update -y
sudo yum install docker -y
sudo usermod -aG docker $USER
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl"
chmod +x kubectl
sudo mv ./kubectl /usr/bin/kubectl
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-arm64
chmod +x ./kind
sudo mv ./kind /usr/bin/kind
sudo systemctl start docker 
sudo systemctl enable docker
