#!/bin/sh -xe
sudo yum update -y
sudo yum install docker -y
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x kubectl
sudo mv ./kubectl /usr/bin/kubectl
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/bin/kind
sudo systemctl start docker 
sudo systemctl enable docker
sudo usermod -aG docker $USER
curl --location --request GET "https://gitlab.com/api/v4/projects/43085800/repository/files/kubernetes%2Fkind_config.yaml/raw?ref=main" >/home/ec2-user/kind_config.yaml
kind create cluster --name test --config kind_config.yaml
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.1/manifests/calico.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
kubectl wait --namespace metallb-system \
--for=condition=ready pod \
--selector=app=metallb \
--timeout=90s
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx \
--for=condition=ready pod \
--selector=app.kubernetes.io/component=controller \
--timeout=90s
curl --location --request GET "https://gitlab.com/api/v4/projects/43085800/repository/files/kubernetes%2Fall_in_one_for_create_k8s_resource.yaml/raw?ref=main" >/home/ec2-user/test_deployment.yaml
kubectl apply -f test_deployment.yaml
docker ps -a
kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous
