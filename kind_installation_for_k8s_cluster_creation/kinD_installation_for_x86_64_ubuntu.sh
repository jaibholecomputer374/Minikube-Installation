#!/bin/bash

sudo apt-get install docker.io -y

sudo service docker start

sudo service docker enable

sudo usermod -aG docker ubuntu

newgrp docker

 
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

sudo install -o ubuntu -g ubuntu -m 0755 kubectl /usr/local/bin/kubectl

sudo rm -rf kubectl

[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64

sudo install -o ubuntu -g ubuntu -m 0755 kind /usr/local/bin/kind

sudo rm -rf kind

curl --location --request GET "https://gitlab.com/api/v4/projects/43085800/repository/files/kubernetes%2Fkind_config.yaml/raw?ref=main" >/home/ubuntu/kind_config.yaml


kind create cluster --name test --config kind_config.yaml

kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.1/manifests/calico.yaml

kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml

kubectl wait --namespace metallb-system \
--for=condition=ready pod \
--selector=app=metallb \
--timeout=90s

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx \
--for=condition=ready pod \
--selector=app.kubernetes.io/component=controller \
--timeout=90s


