#!/bin/bash

# Variables
USER_NAME="admin"
KUBE_API_SERVER="https://$(dig +short myip.opendns.com @resolver1.opendns.com):6443"
KUBE_CONFIG_FILE="${USER_NAME}.kubeconfig"

# Generate a private key
openssl genpkey -algorithm RSA -out ${USER_NAME}.key -pkeyopt rsa_keygen_bits:2048

# Create a CSR
openssl req -new -key ${USER_NAME}.key -out ${USER_NAME}.csr -subj "/CN=${USER_NAME}"

# Copy the Kubernetes CA certificate and key (adjust the path as necessary)
cp /etc/kubernetes/pki/ca.crt .
cp /etc/kubernetes/pki/ca.key .

# Sign the CSR with the Kubernetes CA
openssl x509 -req -in ${USER_NAME}.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out ${USER_NAME}.crt -days 365

# Base64 encode the certificates and key
CA_CERT=$(cat ca.crt | base64 -w 0)
USER_CERT=$(cat ${USER_NAME}.crt | base64 -w 0)
USER_KEY=$(cat ${USER_NAME}.key | base64 -w 0)

# Create kubeconfig file
cat <<EOF > ${KUBE_CONFIG_FILE}
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: ${CA_CERT}
    server: ${KUBE_API_SERVER}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: ${USER_NAME}
  name: ${USER_NAME}-context
current-context: ${USER_NAME}-context
users:
- name: ${USER_NAME}
  user:
    client-certificate-data: ${USER_CERT}
    client-key-data: ${USER_KEY}
EOF

# Create ClusterRoleBinding for the user
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: ${USER_NAME}-cluster-admin-binding
subjects:
- kind: User
  name: ${USER_NAME}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
EOF

echo "User '${USER_NAME}' created with cluster-admin role."
echo "Kubeconfig file '${KUBE_CONFIG_FILE}' generated for the user to access the cluster."