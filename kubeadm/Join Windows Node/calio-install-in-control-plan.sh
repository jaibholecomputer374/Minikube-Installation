#!/bin/bash

#Install the Tigera Calico operator and custom resource definitions

CALICO_VERSION="v3.28.0"

# Install the Tigera Calico operator
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/$CALICO_VERSION/manifests/tigera-operator.yaml

# Download custom resources YAML file
curl -O https://raw.githubusercontent.com/projectcalico/calico/$CALICO_VERSION/manifests/custom-resources.yaml

# Define the new CIDR value for service
new_cidr="10.96.0.0/12"

# Path to the YAML file
yaml_file="./custom-resources.yaml"

# Use awk to find and replace the CIDR value in the custom resources YAML
awk -v new_cidr="$new_cidr" '/cidr:/ {sub($2, new_cidr)} 1' "$yaml_file" > temp.yaml && mv temp.yaml "$yaml_file"

# Apply the modified custom resources YAML file
kubectl apply -f ${yaml_file}

# Download and configure calicoctl
curl -L https://github.com/projectcalico/calico/releases/download/$CALICO_VERSION/calicoctl-linux-amd64 -o calicoctl
chmod +x ./calicoctl
sudo mv calicoctl /usr/local/bin/calicoctl

# Configure calicoctl IPAM settings
cd /usr/local/bin
./calicoctl ipam configure --strictaffinity=true --allow-version-mismatch

# Wait for Calico pods to be ready
kubectl wait --namespace calico-system --for=condition=ready pod --timeout=60s

# Check Calico pods status
kubectl get pods -n calico-system

# Taint control plane nodes to prevent pod scheduling
kubectl taint nodes --all node-role.kubernetes.io/control-plane=NoSchedule:NoSchedule

# List nodes to verify taints
kubectl get nodes -o wide

# Additional commands or checks can be added as needed
