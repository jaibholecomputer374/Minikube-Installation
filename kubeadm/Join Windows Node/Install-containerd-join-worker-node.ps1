# Define network CIDRs
$podNetworkCidr = "10.244.0.0/16"
$serviceCidr = "10.96.0.0/12"

# Create the necessary directory
if (-not (Test-Path -Path "C:\k")) {
    mkdir C:\k -Force
}

# Download the install-containerd-runtime script
try {
    Invoke-WebRequest -UseBasicParsing "https://raw.githubusercontent.com/microsoft/Windows-Containers/Main/helpful_tools/Install-ContainerdRuntime/install-containerd-runtime.ps1" -OutFile "C:\k\install-containerd-runtime.ps1"
    Write-Host "Downloaded install-containerd-runtime.ps1 successfully."
} catch {
    Write-Error "Failed to download install-containerd-runtime.ps1: $_"
    exit 1
}

# Execute the install-containerd-runtime script
try {
    & "C:\k\install-containerd-runtime.ps1"
    Write-Host "Executed install-containerd-runtime.ps1 successfully."
} catch {
    Write-Error "Failed to execute install-containerd-runtime.ps1: $_"
    exit 1
}

# Create the config file
New-Item -Path "C:\k\config" -ItemType "File" -Force

# Prompt the user to edit the config file
Write-Host "Please copy the content of the config file from the CONTROL PLANE and paste it into the opened Notepad. FYI, you can get the content by using 'cat /root/.kube/config' on the control plane."

# Open the config file in Notepad and wait for the user to save and close it
$notepadProcess = Start-Process -FilePath notepad.exe -ArgumentList "C:\k\config" -PassThru
Wait-Process -Id $notepadProcess.Id

# Continue with the rest of the script
Write-Host "Notepad closed. Continuing with the script..."

# Download the Calico install script for Windows
try {
    Invoke-WebRequest -Uri "https://github.com/projectcalico/calico/releases/download/v3.28.0/install-calico-windows.ps1" -OutFile "C:\k\install-calico-windows.ps1"
    Write-Host "Downloaded install-calico-windows.ps1 successfully."
} catch {
    Write-Error "Failed to download install-calico-windows.ps1: $_"
    exit 1
}

# Execute the Calico install script for Windows
try {
    & "C:\k\install-calico-windows.ps1" -ReleaseBaseURL "https://github.com/projectcalico/calico/releases/download/v3.28.0" -ReleaseFile "calico-windows-v3.28.0.zip" -KubeVersion "1.29.6" -DownloadOnly "yes" -ServiceCidr $serviceCidr -DNSServerIPs "127.0.0.1"
    Write-Host "Executed install-calico-windows.ps1 successfully."
} catch {
    Write-Error "Failed to execute install-calico-windows.ps1: $_"
    exit 1
}

# Execute the Kubernetes services install script
try {
    & "C:\calicowindows\kubernetes\install-kube-services.ps1"
    Write-Host "Executed install-kube-services.ps1 successfully."
} catch {
    Write-Error "Failed to execute install-kube-services.ps1: $_"
    exit 1
}

# Add firewall rule for Kubelet
try {
    New-NetFirewallRule -Name 'Kubelet-In-TCP' -DisplayName 'Kubelet (node)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 10250
    Write-Host "Added firewall rule for Kubelet successfully."
} catch {
    Write-Error "Failed to add firewall rule for Kubelet: $_"
    exit 1
}

# Start the kubelet and kube-proxy services
try {
    Start-Service kubelet
    Write-Host "Started kubelet service successfully."
    Start-Service kube-proxy
    Write-Host "Started kube-proxy service successfully."
} catch {
    Write-Error "Failed to start services: $_"
    exit 1
}
