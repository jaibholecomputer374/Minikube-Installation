# Define network CIDRs
$podNetworkCidr = "10.244.0.0/16"
$serviceCidr = "10.96.0.0/12"

# Download the install-containerd-runtime script
Invoke-WebRequest -UseBasicParsing "https://raw.githubusercontent.com/microsoft/Windows-Containers/Main/helpful_tools/Install-ContainerdRuntime/install-containerd-runtime.ps1" -OutFile "C:\k\install-containerd-runtime.ps1"

# Execute the install-containerd-runtime script
& "C:\k\install-containerd-runtime.ps1"

# Create the necessary directory and file
mkdir C:\k -Force
New-Item -Path "C:\k\config" -ItemType "File" -Force

# Prompt the user to edit the config file
Write-Host "Please copy the content of the config file from the CONTROL PLANE and paste it into the opened Notepad. FYI, you can get the content by using 'cat /root/.kube/config' on the control plane."

# Open the config file in Notepad and wait for the user to save and close it
$notepadProcess = Start-Process -FilePath notepad.exe -ArgumentList "C:\k\config" -PassThru
Wait-Process -Id $notepadProcess.Id

# Continue with the rest of the script
Write-Host "Notepad closed. Continuing with the script..."

# Download the Calico install script for Windows
Invoke-WebRequest -Uri "https://github.com/projectcalico/calico/releases/download/v3.28.0/install-calico-windows.ps1" -OutFile "C:\k\install-calico-windows.ps1"

# Execute the Calico install script for Windows
& "C:\k\install-calico-windows.ps1" -ReleaseBaseURL "https://github.com/projectcalico/calico/releases/download/v3.28.0" -ReleaseFile "calico-windows-v3.28.0.zip" -KubeVersion "1.29.6" -DownloadOnly "yes" -ServiceCidr $serviceCidr -DNSServerIPs "127.0.0.1"


c:\calicowindows\kubernetes\install-kube-services.ps1

New-NetFirewallRule -Name 'Kubelet-In-TCP' -DisplayName 'Kubelet (node)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 10250

Start-Service kubelet

Start-Service kube-proxy

