podNetworkCidr = "10.244.0.0/16" 
serviceCidr = "10.96.0.0/12"

sudo swapoff -a
sudo apt update -y
echo "Make script executable using chmod u+x FILE_NAME.sh"

echo "Containerd installation script"
echo "Instructions from https://kubernetes.io/docs/setup/production-environment/container-runtimes/"

echo "Creating containerd configuration file with list of necessary modules that need to be loaded with containerd"
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

echo "Load containerd modules"
sudo modprobe overlay
sudo modprobe br_netfilter


echo "Creates configuration file for kubernetes-cri file (changed to k8s.conf)"
# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

echo "Applying sysctl params"
sudo sysctl --system


echo "Verify that the br_netfilter, overlay modules are loaded by running the following commands:"
lsmod | grep br_netfilter
lsmod | grep overlay

echo "Verify that the net.bridge.bridge-nf-call-iptables, net.bridge.bridge-nf-call-ip6tables, and net.ipv4.ip_forward system variables are set to 1 in your sysctl config by running the following command:"
sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward

echo "Update packages list"
sudo apt-get update

echo "Install containerd"
sudo apt-get -y install containerd

echo "Create a default config file at default location"
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml

echo "Restarting containerd"
sudo systemctl restart containerd


echo "Make script executable using chmod u+x FILE_NAME.sh"

sudo apt-get update

# apt-transport-https may be a dummy package; if so, you can skip that package
sudo apt-get install -y apt-transport-https ca-certificates curl gpg


curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update

echo "Installing latest versions"
sudo apt-get install -y kubelet kubeadm kubectl

echo "Fixate version to prevent upgrades"
sudo apt-mark hold kubelet kubeadm kubectl

kubeadm version

echo "Creating kubernetes cluster"

sudo kubeadm init  --apiserver-cert-extra-sans=$(hostname -I | tr ' ' ',')$(dig +short myip.opendns.com @resolver1.opendns.com) --pod-network-cidr=$podNetworkCidr --service-cidr=$serviceCidr

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

sudo kubectl -n kube-system get pod --kubeconfig /etc/kubernetes/admin.conf

kubectl get pod -A

# Backup the original file
cp /etc/containerd/config.toml /etc/containerd/config.toml.bak

# Change the value of SystemdCgroup to true
sed -i '/SystemdCgroup *= *false/c\            SystemdCgroup = true' /etc/containerd/config.toml

# Verify the change
grep 'SystemdCgroup' /etc/containerd/config.toml

echo "Updated SystemdCgroup to true in /etc/containerd/config.toml"

service containerd restart

# Variables
USER_NAME="admin"
KUBE_API_SERVER="https://$(dig +short myip.opendns.com @resolver1.opendns.com):6443"
KUBE_CONFIG_FILE="${USER_NAME}.kubeconfig"

# Generate a private key
openssl genpkey -algorithm RSA -out ${USER_NAME}.key -pkeyopt rsa_keygen_bits:2048

# Create a CSR
openssl req -new -key ${USER_NAME}.key -out ${USER_NAME}.csr -subj "/CN=${USER_NAME}"

# Copy the Kubernetes CA certificate and key (adjust the path as necessary)
cp /etc/kubernetes/pki/ca.crt .
cp /etc/kubernetes/pki/ca.key .

# Sign the CSR with the Kubernetes CA
openssl x509 -req -in ${USER_NAME}.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out ${USER_NAME}.crt -days 365

# Base64 encode the certificates and key
CA_CERT=$(cat ca.crt | base64 -w 0)
USER_CERT=$(cat ${USER_NAME}.crt | base64 -w 0)
USER_KEY=$(cat ${USER_NAME}.key | base64 -w 0)

# Create kubeconfig file
cat <<EOF > ${KUBE_CONFIG_FILE}
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: ${CA_CERT}
    server: ${KUBE_API_SERVER}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: ${USER_NAME}
  name: ${USER_NAME}-context
current-context: ${USER_NAME}-context
users:
- name: ${USER_NAME}
  user:
    client-certificate-data: ${USER_CERT}
    client-key-data: ${USER_KEY}
EOF

# Create ClusterRoleBinding for the user
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: ${USER_NAME}-cluster-admin-binding
subjects:
- kind: User
  name: ${USER_NAME}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
EOF

echo "User '${USER_NAME}' created with cluster-admin role."
echo "Kubeconfig file '${KUBE_CONFIG_FILE}' generated for the user to access the cluster."


echo "Comment this in script if you want to use WINDOWS Worker Node"
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/canal.yaml

cat ${KUBE_CONFIG_FILE}

kubeadm token create --print-join-command



