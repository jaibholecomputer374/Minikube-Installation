If you missed that command then you can generate join command again by executing the following command from control plane:

kubeadm token create --print-join-command



3.	Install a pod network (e.g., Flannel):

kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/calico.yaml


To prevent crash loop containers, we will apply the same fix to containerd conf file that we did in control plane node.

Edit /etc/containerd/config.toml file from control plane node

version = 2
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
  **SystemdCgroup = true**


**sudo vim /etc/containerd/config.toml**

 Restart containerd and kubelet services

**sudo service containerd restart && sudo service kubelet restart**

Stop port:
**sudo netstat -tuln | grep 6443

 lsof -i :10257

**

Uninstall Cluster

kubeadm reset -f

/*On Debian base Operating systems you can use the following command.*/
# on debian base 

sudo apt-get purge kubeadm kubectl kubelet kubernetes-cni kube* 


/*On CentOs distribution systems you can use the following command.*/
#on centos base


sudo yum remove kubeadm kubectl kubelet kubernetes-cni kube*


# on debian base


sudo apt-get autoremove

#on centos base

sudo yum autoremove

/For all/

sudo rm -rf ~/.kube

###################
**kubeadm reset

sudo apt-get purge kubeadm kubectl kubelet kubernetes-cni kube*   

sudo apt-get autoremove  

sudo rm -rf ~/.kube**


